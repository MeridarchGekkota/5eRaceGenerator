import random
import re
import copy

###
# Developed by MeridarchGekkota using
# Musicus Meter rules for 5e.
###
class RaceGenerator:
    ###
    # Traits are the main backbone of this.
    # They require (at minimum) a name, a description
    # and a cost.
    #
    # The description can include [Bracketed] text
    # which then references to some other item stored
    # within the trait itself, another element of
    # randomisation.
    #
    # [Option-Thing] is a more complex bit of functionality
    # where the Option is linked to one or more pieces of
    # replacement text and possibly an optional cost.
    #
    # This allows for grouping of multiple different levels
    # of customisation without having to worry about those
    # traits having a higher chance of selection.
    ###
    traits = {
                 "Tiny": {
            "Name": "Tiny",
            "Description": "Your size is Tiny.",
            "Point Cost": -2,
            "Prevents Traits": {
                "Small",
            }
        },
                 "Damage Vulnerability": {
            "Name": "Damage Vulnerability",
            "Description": "You have vulnerability to [Damage Type] damage.",
            "Point Cost": -1,
            "Damage Type": [
                "Acid", "Bludgeoning", "Cold", "Fire", "Force", "Lightning", "Necrotic", "Piercing", "Poison",
                "Psychic", "Radiant", "Slashing", "Thunder"
            ]
        },
                 "Small": {
            "Name": "Small",
            "Description": "Your size is Small.",
            "Point Cost": -1,
            "Prevents Traits": {
                "Tiny",
            }
        },
                 "Sunlight Sensitivity": {
            "Name": "Sunlight Sensitivity",
            "Description": "You have disadvantage on attack rolls and on "
                           "Wisdom (Perception) checks that rely "
                           "on sight when you, the target of your attack, or "
                           "whatever you are trying to perceive is in direct "
                           "sunlight.",
            "Point Cost": -0.5
        },
                 "Ability Swapping": {
            "Name": "Ability Swapping",
            "Description": "You can use your {ability score} "
                           "modifier instead of your {ability score} "
                           "modifier when {situation}.",
            "Point Cost": 0.5
        },
                 "Amphibious": {
            "Name": "Amphibious",
            "Description": "You can breathe air and water.",
            "Point Cost": 0.5
        },
                 "Armor Proficiency": {
            "Name": "Armor Proficiency",
            "Description": "You have proficiency with [Armor].",
            "Point Cost": 0.5,
            "Armor": [
                "Light Armor",
                "Medium Armor",
                "Heavy Armor",
                "Shields"
            ]
        },
                 "Brave": {
            "Name": "Brave",
            "Description": "You have advantaged on saving throws against "
                           "being Frightened.",
            "Point Cost": 0.5
        },
                 "Cantrip": {
            "Name": "Cantrip",
            "Description": "You know [Cantrip]. [Ability Score] is your "
                           "spellcasting ability for it.",
            "Point Cost": 0.5,
            "Cantrip": [
                "one cantrip of your choice from the Bard spell list",
                "one cantrip of your choice from the Cleric spell list",
                "one cantrip of your choice from the Druid spell list",
                "one cantrip of your choice from the Sorcerer spell list",
                "one cantrip of your choice from the Warlock spell list",
                "one cantrip of your choice from the Wizard spell list",
                "the Acid Splash cantrip",
                "the Blade Ward cantrip",
                "the Chill Touch cantrip",
                "the Dancing Lights cantrip",
                "the Druidcraft cantrip",
                "the Eldritch Blast cantrip",
                "the Fire Bolt cantrip",
                "the Friends cantrip",
                "the Guidance cantrip",
                "the Light cantrip",
                "the Mage Hand cantrip",
                "the Mending cantrip",
                "the Message cantrip",
                "the Minor Illusion cantrip",
                "the Poison Spray cantrip",
                "the Prestidigitation cantrip",
                "the Produce Flame cantrip",
                "the Ray of Frost cantrip",
                "the Resistance cantrip",
                "the Sacred Flame cantrip",
                "the Shillelagh cantrip",
                "the Shocking Grasp cantrip",
                "the Spare the Dying cantrip",
                "the Thaumaturgy cantrip",
                "the Thorn Whip cantrip",
                "the True Strike cantrip",
                "the Vicious Mockery cantrip"
            ],
            "Ability Score":  [
                "Strength", "Dexterity", "Constitution", "Intelligence", "Wisdom", "Charisma"
            ]
        },
                 "Climb": {
            "Name": "Climb",
            "Description": "You have a climbing speed of [Option-Feet] feet.",
            "Point Cost": "[Option-Cost]",
            "Option": [
                {
                    "Feet": 10,
                    "Cost": 0.5
                },
                {
                    "Feet": 20,
                    "Cost": 1
                },
                {
                    "Feet": 30,
                    "Cost": 1.5
                },
                {
                    "Feet": 40,
                    "Cost": 2
                }
            ]
        },
                 "Damage Resistance": {
            "Name": "Damage Resistance",
            "Description": "You have resistance to [Damage Type] damage.",
            "Point Cost": 0.5,
            "Damage Type": [
                "Acid", "Bludgeoning", "Cold", "Fire", "Force", "Lightning", "Necrotic", "Piercing", "Poison",
                "Psychic", "Radiant", "Slashing", "Thunder"
            ]
        },
                 "Darkvision": {
            "Name": "Darkvision",
            "Description": "You can see in Dim Light within 60 feet of you "
                           "as if it were Bright Light, and in Darkness as "
                           "if it were Dim Light. You can't discern color "
                           "in Darkness, only shades of gray.",
            "Point Cost": 0.5
        },
                 "Environmental Adaptation": {
            "Name": "Environmental Adaptation",
            "Description": "You're acclimated to [Environment], as described in "
                           "chapter 5 of the Dungeon Master's Guide.",
            "Point Cost": 0.5,
            "Environment": [
                "Forest", "Jungle", "Swamp", "Mountains", "Open Sea", "Arctic", "Desert", "Grassland"
            ]
        },
                 "Extra Language": {
            "Name": "Extra Language",
            "Description": "You can speak, read, and write one extra language "
                           "of your choice.",
            "Point Cost": 0.5
        },
                 "Powerful Build": {
            "Name": "Powerful Build",
            "Description": "You count as one size larger when determining your "
                           "carrying capacity and the weight you can push, drag, "
                           "or lift.",
            "Point Cost": 0.5
        },
                 "Extra Movement": {
            "Name": "Extra Movement",
            "Description": "Your base walking speed increases by [Option-Feet] feet.",
            "Point Cost": "Option-Cost",
            "Option": [
                {
                    "Feet": 5,
                    "Cost": 0.5
                },
                {
                    "Feet": 10,
                    "Cost": 1
                }
            ]
        },
                 "Fey Ancestry": {
            "Name": "Fey Ancestry",
            "Description": "You have advantage on saving throws against being "
                           "Charmed and magic can't put you to sleep.",
            "Point Cost": 0.5
        },
                 "Innate Casting": {
            # 1st level spell version
            # too many options otherwise
            "Name": "Innate Casting",
            "Description": "You can cast the [Spell] spell once with "
                           "this trait, requiring no material components, and you "
                           "regain the ability to cast it this way when you finish "
                           "a long rest. [Ability Score] is your "
                           "spellcasting ability for this spell.",
            "Point Cost": 0.5,
            "Spell": [
                "Alarm",
                "Animal Friendship",
                "Armor of Agathys",
                "Arms of Hadar",
                "Bane",
                "Bless",
                "Burning Hands",
                "Charm Person",
                "Chromatic Orb",
                "Color Spray",
                "Command",
                "Compelled Duel",
                "Comprehend Languages",
                "Create or Destroy Water",
                "Cure Wounds",
                "Detect Evil and Good",
                "Detect Magic",
                "Detect Poison and Disease",
                "Disguise Self",
                "Dissonant Whispers",
                "Divine Favor",
                "Ensnaring Strike",
                "Entangle",
                "Expeditious Retreat",
                "Faerie Fire",
                "False Life",
                "Feather Fall",
                "Find Familiar",
                "Fog Cloud",
                "Goodberry",
                "Grease",
                "Guiding Bolt",
                "Hail of Thorns",
                "Healing Word",
                "Hellish Rebuke",
                "Heroism",
                "Hex",
                "Hunter’s Mark",
                "Identify",
                "Illusory Script",
                "Inflict Wounds",
                "Jump",
                "Longstrider",
                "Mage Armor",
                "Magic Missile",
                "Protection from Evil and Good",
                "Purify Food and Drink",
                "Ray of Sickness",
                "Sanctuary",
                "Searing Smite",
                "Shield",
                "Shield of Faith",
                "Silent Image",
                "Sleep",
                "Speak with Animals",
                "Tasha’s Hideous Laughter",
                "Tenser’s Floating Disk",
                "Thunderous Smite",
                "Thunderwave",
                "Unseen Servant",
                "Witch Bolt",
                "Wrathful Smite"
            ],
            "Ability Score":  [
                "Strength", "Dexterity", "Constitution", "Intelligence", "Wisdom", "Charisma"
            ]
        },
                 "Innate Casting": {
            # 2nd level spell version
            # too many options otherwise
            "Name": "Innate Casting",
            "Description": "You can cast the [Spell] spell once with "
                           "this trait, requiring no material components, and you "
                           "regain the ability to cast it this way when you finish "
                           "a long rest. [Ability Score] is your "
                           "spellcasting ability for this spell.",
            "Point Cost": 1,
            "Spell": [
                "Aid",
                "Alter Self",
                "Animal Messenger",
                "Arcane Lock",
                "Augury",
                "Barkskin",
                "Beast Sense",
                "Blindness / Deafness",
                "Blur",
                "Branding Smite",
                "Calm Emotions",
                "Cloud of Daggers",
                "Continual Flame",
                "Cordon of Arrows",
                "Crown of Madness",
                "Darkness",
                "Darkvision",
                "Detect Thoughts",
                "Enhance Ability",
                "Enlarge / Reduce",
                "Enthrall",
                "Find Steed",
                "Find Traps",
                "Flame Blade",
                "Flaming Sphere",
                "Gentle Repose",
                "Gust of Wind",
                "Heat Metal",
                "Hold Person",
                "Invisibility",
                "Knock",
                "Lesser Restoration",
                "Levitate",
                "Locate Animals or Plants",
                "Locate Object",
                "Magic Mouth",
                "Magic Weapon",
                "Melf’s Acid Arrow",
                "Mirror Image",
                "Misty Step",
                "Moonbeam",
                "Nystul’s Magic Aura",
                "Pass without Trace",
                "Phantasmal Force",
                "Prayer of Healing",
                "Protection from Poison",
                "Ray of Enfeeblement",
                "Rope Trick",
                "Scorching Ray",
                "See Invisibility",
                "Shatter",
                "Silence",
                "Spider Climb",
                "Spike Growth",
                "Spiritual Weapon",
                "Suggestion",
                "Warding Bond",
                "Web",
                "Zone of Truth"
            ],
            "Ability Score":  [
                "Strength", "Dexterity", "Constitution", "Intelligence", "Wisdom", "Charisma"
            ]
            # 0.5 cost per 1 spell level
            #TODO:// Add all this in, including description
        },
                 "Innate Casting": {
            # 3rd level spell version
            # too many options otherwise
            "Name": "Innate Casting",
            "Description": "You can cast the [Spell] spell once with "
                           "this trait, requiring no material components, and you "
                           "regain the ability to cast it this way when you finish "
                           "a long rest. [Ability Score] is your "
                           "spellcasting ability for this spell.",
            "Point Cost": 1.5,
            "Spell": [
                "Animate Dead",
                "Aura of Vitality",
                "Beacon of Hope",
                "Bestow Curse",
                "Blinding Smite",
                "Blink",
                "Call Lightning",
                "Clairvoyance",
                "Conjure Animals",
                "Conjure Barrage",
                "Counterspell",
                "Create Food and Water",
                "Crusader’s Mantle",
                "Daylight",
                "Dispel Magic",
                "Elemental Weapon",
                "Fear",
                "Feign Death",
                "Fireball",
                "Fly",
                "Gaseous Form",
                "Glyph of Warding",
                "Haste",
                "Hunger of Hadar",
                "Hypnotic Pattern",
                "Leomund’s Tiny Hut",
                "Lightning Arrow",
                "Lightning Bolt",
                "Magic Circle",
                "Major Image",
                "Mass Healing Word",
                "Meld into Stone",
                "Nondetection",
                "Phantom Steed",
                "Plant Growth",
                "Protection from Energy",
                "Remove Curse",
                "Revivify",
                "Sending",
                "Sleet Storm",
                "Slow",
                "Speak with Dead",
                "Speak with Plants",
                "Spirit Guardians",
                "Stinking Cloud",
                "Tongues",
                "Vampiric Touch",
                "Water Breathing",
                "Water Walk",
                "Wind Wall"
            ],
            "Ability Score":  [
                "Strength", "Dexterity", "Constitution", "Intelligence", "Wisdom", "Charisma"
            ]
            # 0.5 cost per 1 spell level
            #TODO:// Add all this in, including description
        },
                 "Lucky": {
            "Name": "Lucky",
            "Description": "When you roll a 1 on an attack roll, ability "
                           "check, or saving throw, you can re-roll the "
                           "die and must use the new roll.",
            "Point Cost": 0.5
        },
                 "Mask of the Wild": {
            "Name": "Mask of the Wild",
            "Description": "You can attempt to hide even when you are only "
                           "lightly obscured by foliage, heavy rain, falling "
                           "snow, mist, and other natural phenomena.",
            "Point Cost": 0.5
        },
                 "Natural Armor": {
            "Name": "Natural Armor",
            "Description": "When you aren't wearing armor, your AC is [Option-AC] + your "
                           "Dex modifier. You can use your natural armor "
                           "to determine your AC if the armor you wear would leave "
                           "you with a lower AC. A shield's benefits apply as normal "
                           "while you use your natural armor.",
            "Point Cost": "[Option-Cost]",
            "Option": [
                {
                    "AC": 11,
                    "Cost": 0.5
                },
                {
                    "AC": 12,
                    "Cost": 1
                },
                {
                    "AC": 13,
                    "Cost": 2
                }
            ]
        },
                 "Natural Weapon": {
            "Name": "Natural Weapon",
            "Description": "Your [Option-Body Part] [Option-Verb Noun], which you can "
                           "use to make [Option-Attack Type]. If you hit with them, you deal "
                           "[Option-Damage Type] damage equal to [Option-Damage] + "
                           "your Str or Dex modifier, instead of the "
                           "bludgeoning damage normal for an unarmed strike.",
            "Point Cost": "Option-Cost",
            "Option": [
                {
                    "Body Part": "claws",
                    "Verb Noun": "are natural weapons",
                    "Attack Type": "unarmed strikes",
                    "Damage": "1d4",
                    "Damage Type": "slashing",
                    "Cost": 0.5
                },
                {
                    "Body Part": "talons",
                    "Verb Noun": "are natural weapons",
                    "Attack Type": "unarmed strikes",
                    "Damage": "1d4",
                    "Damage Type": "slashing",
                    "Cost": 0.5
                },
                {
                    "Body Part": "horns",
                    "Verb Noun": "are natural weapons",
                    "Attack Type": "unarmed strikes",
                    "Damage": "1d4",
                    "Damage Type": "bludgeoning",
                    "Cost": 0.5
                },
                {
                    "Body Part": "fangs",
                    "Verb Noun": "are natural weapons",
                    "Attack Type": "unarmed strikes",
                    "Damage": "1d4",
                    "Damage Type": "bludgeoning",
                    "Cost": 0.5
                },
                {
                    "Body Part": "quills",
                    "Verb Noun": "are natural ranged weapons with a range of 20 feet",
                    "Attack Type": "ranged attacks",
                    "Damage": "1d4",
                    "Damage Type": "piercing",
                    "Cost": 0.5
                },
                {
                    "Body Part": "claws",
                    "Verb Noun": "are natural weapons",
                    "Attack Type": "unarmed strikes",
                    "Damage": "1d6",
                    "Damage Type": "slashing",
                    "Cost": 1
                },
                {
                    "Body Part": "talons",
                    "Verb Noun": "are natural weapons",
                    "Attack Type": "unarmed strikes",
                    "Damage": "1d6",
                    "Damage Type": "slashing",
                    "Cost": 1
                },
                {
                    "Body Part": "horns",
                    "Verb Noun": "are natural weapons",
                    "Attack Type": "unarmed strikes",
                    "Damage": "1d6",
                    "Damage Type": "bludgeoning",
                    "Cost": 1
                },
                {
                    "Body Part": "quills",
                    "Verb Noun": "are natural ranged weapons with a range of 30 feet",
                    "Attack Type": "ranged attacks",
                    "Damage": "1d6",
                    "Damage Type": "piercing",
                    "Cost": 0.5
                },
            ]
        },
                 "Nimble": {
            "Name": "Nimble",
            "Description": "You can move through the space of any creature that is "
                           "of a size larger than yours.",
            "Point Cost": 0.5
        },
                 "Poison Resilience": {
            "Name": "Poison Resilience",
            "Description": "You have Advantage on saving throws against Poison, and "
                           "you have resistance against Poison damage.",
            "Point Cost": 0.5
        },
                 "Savage Attacks": {
            "Name": "Savage Attacks",
            "Description": "When you score a critical hit with a melee weapon attack, "
                           "you can roll one of the weapon’s damage dice one additional "
                           "time and add it to the extra damage of the critical hit.",
            "Point Cost": 0.5
        },
                 "Skill Proficiency": {
            "Name": "Skill Proficiency",
            "Description": "You have proficiency in the [Skill] skill.",
            "Point Cost": 0.5,
            "Skill": [
                # Strength Skills
                "Athletics",
                # Dexterity Skills
                "Acrobatics", "Sleight of Hand", "Stealth",
                # Intelligence Skills
                "Arcana", "History", "Investigation", "Nature", "Religion",
                # Wisdom Skills
                "Animal Handling", "Insight", "Medicine", "Perception", "Survival",
                # Charisma Skills
                "Deception", "Intimidation", "Performance", "Persuasion"
            ],
        },
                 "Speak with Small Beasts": {
            "Name": "Speak with Small Beasts",
            "Description": "Through sounds and gestures, you can "
                           "communicate simple ideas with Small or "
                           "smaller beasts.",
            "Point Cost": 0.5
            # maybe add options for plants
        },
                 "Stonecunning": {
            "Name": "Stonecunning",
            "Description": "Whenever you make a Int (History) check "
                           "related to the origin of stonework, you "
                           "are considered proficient in the History "
                           "skill and add double the proficiency bonus "
                           "to the check, instead of your normal proficiency "
                           "bonus.",
            "Point Cost": 0.5
            # maybe add other options
        },
                 "Swim": {
            "Name": "Swim",
            "Description": "You have a swimming speed of [Option-Feet] feet.",
            "Point Cost": "[Option-Cost]",
            "Option": [
                {
                    "Feet": 10,
                    "Cost": 0.5
                },
                {
                    "Feet": 20,
                    "Cost": 1
                },
                {
                    "Feet": 30,
                    "Cost": 1.5
                },
                {
                    "Feet": 40,
                    "Cost": 2
                }
            ]
        },
                 "Tinker": {
            "Name": "Tinker",
            "Description": "You have proficiency with artisan’s tinker's tools. "
                           "Using those tools, you can spend 1 hour and 10 gp worth "
                           "of materials to construct a Tiny clockwork device (AC 5, "
                           "1 hp). The device ceases to function after 24 hours (unless "
                           "you spend 1 hour repairing it to keep the device functioning), "
                           "or when you use your action to dismantle it; at that time, you "
                           "can reclaim the materials used to create it. You can have up "
                           "to three such devices active at a time.",
            "Point Cost": 0.5
            # maybe add other options
        },
                 "Tool Proficiency": {
            "Name": "Tool Proficiency",
            "Description": "You gain proficiency with one of "
                           "[Tool1], [Tool2], "
                           "or [Tool3].",
            "Point Cost": 0.5,
            "Tool1": [
                "alchemist's supplies",
                "brewer's supplies",
                "calligrapher's supplies",
                "carpenter's tools",
                "cartographer's tools",
                "cobbler's tools",
                "cook's utensils",
                "glassblower's tools",
                "jeweler's tools",
                "leatherworker's tools",
                "mason's tools",
                "painter's supplies",
                "potter's tools",
                "smith's tools",
                "tinker's tools",
                "weaver's tools",
                "woodcarver's tools"
            ],
            "Tool2": [
                "alchemist's supplies",
                "brewer's supplies",
                "calligrapher's supplies",
                "carpenter's tools",
                "cartographer's tools",
                "cobbler's tools",
                "cook's utensils",
                "glassblower's tools",
                "jeweler's tools",
                "leatherworker's tools",
                "mason's tools",
                "painter's supplies",
                "potter's tools",
                "smith's tools",
                "tinker's tools",
                "weaver's tools",
                "woodcarver's tools"
            ],
            "Tool3": [
                "alchemist's supplies",
                "brewer's supplies",
                "calligrapher's supplies",
                "carpenter's tools",
                "cartographer's tools",
                "cobbler's tools",
                "cook's utensils",
                "glassblower's tools",
                "jeweler's tools",
                "leatherworker's tools",
                "mason's tools",
                "painter's supplies",
                "potter's tools",
                "smith's tools",
                "tinker's tools",
                "weaver's tools",
                "woodcarver's tools"
            ]
        },
                 "Trance": {
            "Name": "Trance",
            "Description": "You don’t need to sleep. Instead, you meditate deeply, "
                           "remaining semiconscious, for 4 hours a day. While "
                           "meditating, you can dream after a fashion; such "
                           "dreams are actually mental exercises that have become "
                           "reflexive through years of practice. After resting "
                           "in this way, you gain the same benefit that a human "
                           "does from 8 hours of sleep. ",
            "Point Cost": 0.5
        },
                 "Weapon Proficiency": {
            "Name": "Weapon Proficiency",
            "Description": "You have proficiency with the [Weapon1], [Weapon2], "
                           "[Weapon3], and [Weapon4].",
            "Point Cost": 0.5,
            "Weapon1": [
                "Club",
                "Dagger",
                "Greatclub",
                "Handaxe",
                "Javelin",
                "Light Hammer",
                "Mace",
                "Quarterstaff",
                "Sickle",
                "Spear",
                "Light Crossbow",
                "Dart",
                "Shortbow",
                "Sling",
                "Battleaxe",
                "Flail",
                "Glaive",
                "Greataxe",
                "Greatsword",
                "Halberd",
                "Lance",
                "Longsword",
                "Maul",
                "Morningstar",
                "Pike",
                "Rapier",
                "Scimitar",
                "Shortsword",
                "Trident",
                "War Pick",
                "Warhammer",
                "Whip",
                "Blowgun",
                "Hand Crossbow",
                "Heavy Crossbow",
                "Longbow",
                "Net"
            ],
            "Weapon2": [
                "Club",
                "Dagger",
                "Greatclub",
                "Handaxe",
                "Javelin",
                "Light Hammer",
                "Mace",
                "Quarterstaff",
                "Sickle",
                "Spear",
                "Light Crossbow",
                "Dart",
                "Shortbow",
                "Sling",
                "Battleaxe",
                "Flail",
                "Glaive",
                "Greataxe",
                "Greatsword",
                "Halberd",
                "Lance",
                "Longsword",
                "Maul",
                "Morningstar",
                "Pike",
                "Rapier",
                "Scimitar",
                "Shortsword",
                "Trident",
                "War Pick",
                "Warhammer",
                "Whip",
                "Blowgun",
                "Hand Crossbow",
                "Heavy Crossbow",
                "Longbow",
                "Net"
            ],
            "Weapon3": [
                "Club",
                "Dagger",
                "Greatclub",
                "Handaxe",
                "Javelin",
                "Light Hammer",
                "Mace",
                "Quarterstaff",
                "Sickle",
                "Spear",
                "Light Crossbow",
                "Dart",
                "Shortbow",
                "Sling",
                "Battleaxe",
                "Flail",
                "Glaive",
                "Greataxe",
                "Greatsword",
                "Halberd",
                "Lance",
                "Longsword",
                "Maul",
                "Morningstar",
                "Pike",
                "Rapier",
                "Scimitar",
                "Shortsword",
                "Trident",
                "War Pick",
                "Warhammer",
                "Whip",
                "Blowgun",
                "Hand Crossbow",
                "Heavy Crossbow",
                "Longbow",
                "Net"
            ],
            "Weapon4": [
                "Club",
                "Dagger",
                "Greatclub",
                "Handaxe",
                "Javelin",
                "Light Hammer",
                "Mace",
                "Quarterstaff",
                "Sickle",
                "Spear",
                "Light Crossbow",
                "Dart",
                "Shortbow",
                "Sling",
                "Battleaxe",
                "Flail",
                "Glaive",
                "Greataxe",
                "Greatsword",
                "Halberd",
                "Lance",
                "Longsword",
                "Maul",
                "Morningstar",
                "Pike",
                "Rapier",
                "Scimitar",
                "Shortsword",
                "Trident",
                "War Pick",
                "Warhammer",
                "Whip",
                "Blowgun",
                "Hand Crossbow",
                "Heavy Crossbow",
                "Longbow",
                "Net"
            ]
        },
                 "Minimum Armor Class": {
            "Name": "Minimum Armor Class",
            "Description": "You Armor Class can not be lower than [Option-AC].",
            "Point Cost": "[Option-Cost]",
            "Option": [
                {
                    "AC": 12,
                    "Cost": 0.5
                },
                {
                    "AC": 14,
                    "Cost": 1.5
                },
                {
                    "AC": 16,
                    "Cost": 3
                }
            ]
        },
                 "Breath Weapon": {
            "Name": "Breath Weapon",
            "Description": " You can use your action to exhale destructive energy. This produces a "
                           "[Shape] dealing [Damage Type] damage. \r\n"
                           "\r\n"
                           "When you use your breath weapon, each creature in the area of the "
                           "exhalation must make a [Saving Throw] saving throw. "
                           "The DC for this saving throw equals 8 + your Con modifier + "
                           "your proficiency bonus. A creature takes 2d6 damage on a failed save, "
                           "and half as much damage on a successful one. The damage increases to "
                           "3d6 at 6th level, 4d6 at 11th level, and 5d6 at 16th level."
                           "\r\n"
                           "After you use your breath weapon, you can’t use it again until you "
                           "complete a short or long rest.",
            "Point Cost": 1,
            "Shape": [
                "15-foot cone",
                "5 by 30-foot line"
            ],
            "Damage Type":  [
                "Acid", "Cold", "Fire", "Force", "Lightning", "Necrotic", "Poison", "Psychic", "Radiant", "Thunder"
            ],
            "Saving Throw":  [
                "Strength", "Dexterity", "Constitution", "Intelligence", "Wisdom", "Charisma"
            ]
        },
                 "Burrow": {
            "Name": "Burrow",
            "Description": "You have a burrow speed of [Option-Feet] feet.",
            "Point Cost": "[Option-Cost]",
            "Option": [
                {
                    "Feet": 15,
                    "Cost": 1
                },
                {
                    "Feet": 30,
                    "Cost": 2
                }
            ]
        },
                 "Living Construct": {
            "Name": "Living Construct",
            "Description": "Even though you were constructed, you are a living "
                           "creature. You are immune to Diseases. You do not need "
                           "to eat or breathe, but you can ingest food and drink "
                           "if you wish. Instead of sleeping, you enter an inactive "
                           "state for 4 hours each day. You do not dream in this state; "
                           "you are fully aware of your surroundings and notice approaching "
                           "enemies and other events as normal.",
            "Point Cost": 1
        },
                 "Minimum Armor Class": {
            "Name": "Minimum Armor Class",
            "Description": "You Armor Class can not be lower than 12.",
            "Point Cost": 0.5
        },
                 "Naturally Stealthy": {
            "Name": "Naturally Stealthy",
            "Description": "You can attempt to hide even when you are "
                           "obscured only by a creature that is at least "
                           "one size larger than you.",
            "Point Cost": 1
        },
                 "Relentless Endurance": {
            "Name": "Relentless Endurance",
            "Description": "When you are reduced to 0 hit points but not "
                           "killed outright, you can drop to 1 hit point "
                           "instead. You can' use this feature again until "
                           "you finish a long rest.",
            "Point Cost": 1
        },
                 "Superior Darkvision": {
            "Name": "Superior Darkvision",
            "Description": "You can see in Dim Light within 60 feet of you "
                           "as if it were Bright Light, and in darkness as if"
                           "it were Dim Light. You can't discner color in "
                           "darkness, only shades of gray.",
            "Point Cost": 1
        },
                 "Quicken": {
            "Name": "Quicken",
            "Description": "Reduces the time cost of something by one stage. "
                           "For example, making a Innate Casting a short rest "
                           "instead of a long rest or being able to take the "
                           "Dodge action, as a bonus action.",
            "Point Cost": 1
            #TODO:// Improve this with options.
        },
                 "Innate Magic": {
            "Name": "Innate Magic",
            "Description": "You know the [Cantrip] cantrip. When you reach 3rd level, "
                           "you can cast the [1st Level Spell] spell once and need to finish a "
                           "long rest before you can cast it again. When you reach 5th level, you "
                           "can also cast the [2nd Level Spell] spell once and need to finish a "
                           "long rest before you can cast it again. [Ability Score] "
                           "is your spellcasting ability for these spells.",
            "Point Cost": 1.5,
            "Cantrip": [
                "the Acid Splash cantrip",
                "the Blade Ward cantrip",
                "the Chill Touch cantrip",
                "the Dancing Lights cantrip",
                "the Druidcraft cantrip",
                "the Eldritch Blast cantrip",
                "the Fire Bolt cantrip",
                "the Friends cantrip",
                "the Guidance cantrip",
                "the Light cantrip",
                "the Mage Hand cantrip",
                "the Mending cantrip",
                "the Message cantrip",
                "the Minor Illusion cantrip",
                "the Poison Spray cantrip",
                "the Prestidigitation cantrip",
                "the Produce Flame cantrip",
                "the Ray of Frost cantrip",
                "the Resistance cantrip",
                "the Sacred Flame cantrip",
                "the Shillelagh cantrip",
                "the Shocking Grasp cantrip",
                "the Spare the Dying cantrip",
                "the Thaumaturgy cantrip",
                "the Thorn Whip cantrip",
                "the True Strike cantrip",
                "the Vicious Mockery cantrip"
            ],
            "1st Level Spell": [
                "Alarm",
                "Animal Friendship",
                "Armor of Agathys",
                "Arms of Hadar",
                "Bane",
                "Bless",
                "Burning Hands",
                "Charm Person",
                "Chromatic Orb",
                "Color Spray",
                "Command",
                "Compelled Duel",
                "Comprehend Languages",
                "Create or Destroy Water",
                "Cure Wounds",
                "Detect Evil and Good",
                "Detect Magic",
                "Detect Poison and Disease",
                "Disguise Self",
                "Dissonant Whispers",
                "Divine Favor",
                "Ensnaring Strike",
                "Entangle",
                "Expeditious Retreat",
                "Faerie Fire",
                "False Life",
                "Feather Fall",
                "Find Familiar",
                "Fog Cloud",
                "Goodberry",
                "Grease",
                "Guiding Bolt",
                "Hail of Thorns",
                "Healing Word",
                "Hellish Rebuke",
                "Heroism",
                "Hex",
                "Hunter’s Mark",
                "Identify",
                "Illusory Script",
                "Inflict Wounds",
                "Jump",
                "Longstrider",
                "Mage Armor",
                "Magic Missile",
                "Protection from Evil and Good",
                "Purify Food and Drink",
                "Ray of Sickness",
                "Sanctuary",
                "Searing Smite",
                "Shield",
                "Shield of Faith",
                "Silent Image",
                "Sleep",
                "Speak with Animals",
                "Tasha’s Hideous Laughter",
                "Tenser’s Floating Disk",
                "Thunderous Smite",
                "Thunderwave",
                "Unseen Servant",
                "Witch Bolt",
                "Wrathful Smite"
            ],
            "2nd Level Spell": [
                "Aid",
                "Alter Self",
                "Animal Messenger",
                "Arcane Lock",
                "Augury",
                "Barkskin",
                "Beast Sense",
                "Blindness / Deafness",
                "Blur",
                "Branding Smite",
                "Calm Emotions",
                "Cloud of Daggers",
                "Continual Flame",
                "Cordon of Arrows",
                "Crown of Madness",
                "Darkness",
                "Darkvision",
                "Detect Thoughts",
                "Enhance Ability",
                "Enlarge / Reduce",
                "Enthrall",
                "Find Steed",
                "Find Traps",
                "Flame Blade",
                "Flaming Sphere",
                "Gentle Repose",
                "Gust of Wind",
                "Heat Metal",
                "Hold Person",
                "Invisibility",
                "Knock",
                "Lesser Restoration",
                "Levitate",
                "Locate Animals or Plants",
                "Locate Object",
                "Magic Mouth",
                "Magic Weapon",
                "Melf’s Acid Arrow",
                "Mirror Image",
                "Misty Step",
                "Moonbeam",
                "Nystul’s Magic Aura",
                "Pass without Trace",
                "Phantasmal Force",
                "Prayer of Healing",
                "Protection from Poison",
                "Ray of Enfeeblement",
                "Rope Trick",
                "Scorching Ray",
                "See Invisibility",
                "Shatter",
                "Silence",
                "Spider Climb",
                "Spike Growth",
                "Spiritual Weapon",
                "Suggestion",
                "Warding Bond",
                "Web",
                "Zone of Truth"
            ],
            "Ability Score": [
                "Strength", "Dexterity", "Constitution", "Intelligence", "Wisdom", "Charisma"
            ]
        },
                 "Toughness": {
            "Name": "Toughness",
            "Description": "Your hit point maximum increases by 1, and "
                           "it increases by 1 every time you gain a level.",
            "Point Cost": 1.5
        },
                "Cunning": {
            "Name": "Cunning",
            "Description": "You have Advantage on all Int, Wis and Cha saving "
                           "throws against magic.",
            "Point Cost": 2
            #TODO:// Maybe additional stuff here?
        },
    }

    attributes = [
        "Strength", "Dexterity", "Constitution", "Intelligence", "Wisdom", "Charisma"
    ]

    def generate_race(self, lowest_race_cost = 5):
        cost = 0
        race_string = ""
        attr_string = ""
        traits_string = ""

        trait_list = list(self.traits.values())
        attribute_list = copy.deepcopy(self.attributes)

        # +2 attribute
        attribute_num = random.randint(0, len(attribute_list)-1)
        attr_string += "Attribute Bonus: "
        attr_string += "+2 "
        # pop ensures we can't get the same attribute twice
        attr_string += attribute_list.pop(attribute_num)
        cost += 2

        attr_string += ", "

        # roll for next attribute
        attribute_num = random.randint(0, len(attribute_list)-1)

        attr_string += "+1 "
        attr_string += attribute_list.pop(attribute_num)
        cost += 1

        attr_string += "\r\n"

        # prevents the same trait
        # being used multiple times
        used_traits = set()

        ###
        # while we haven't hit the recommended
        # cost of 5.5
        ###
        while cost <= lowest_race_cost:
            trait_num = random.randint(0, len(trait_list)-1)

            while trait_list[trait_num]["Name"] in used_traits:
                ###
                # we've hit a trait that is used but hasn't been removed
                # which is in cases of prevents traits
                # we remove this trait and reroll
                ###
                trait_list.pop(trait_num)
                trait_num = random.randint(0, len(trait_list)-1)

            trait = trait_list.pop(trait_num)
            used_traits.add(trait["Name"])

            ###
            # if there are additional prevented traits
            # i.e. if we get tiny, we no longer can get small
            ###
            if trait.get("Prevents Traits", None) is not None:
                used_traits = used_traits.union(trait["Prevents Traits"])

            traits_string += trait["Name"]
            traits_string += ": "

            ###
            # Option implies there are multiple values to be changed
            # based off a single randomisation, including cost.
            #
            # For instance, if we have a cost of 0.5 points per 5 feet
            # movement speed increase then that is an option.
            ###
            if "Option" in trait["Description"]:
                description = trait["Description"]
                values = re.findall("\[Option-(.*?)\]", description)
                option_num = random.randint(0, len(trait["Option"])-1)
                for x in values:
                    option = trait["Option"][option_num][x]
                    description = re.sub("\[Option-" + x + "\]", str(option), description)

                if "Option" in trait["Point Cost"]:
                    cost += trait["Option"][option_num]["Cost"]
                else:
                    cost += trait["Point Cost"]

                # if we still have more bracketed stuff
                if "[" in trait["Description"]:
                    values = re.findall("\[(.*?)\]", description)
                    for x in values:
                        option_num = random.randint(0, len(trait[x])-1)
                        option = trait[x][option_num]
                        description = re.sub("\[" + x + "\]", option, description)
                    traits_string += description
                else:
                    traits_string += description
            ###
            # Other brackets imply one or more randomisations that
            # are independent of each other.
            #
            # For instance, a random cantrip that picks a random
            # ability score.
            #
            # Option does not imply there is nothing for this.
            ###
            elif "[" in trait["Description"]:
                description = trait["Description"]
                values = re.findall("\[(.*?)\]", description)
                for x in values:
                    option_num = random.randint(0, len(trait[x])-1)
                    option = trait[x][option_num]
                    description = re.sub("\[" + x + "\]", option, description)
                traits_string += description
                cost += trait["Point Cost"]

            ###
            # There are no customisations for the currently rolled value.
            ###
            else:
                traits_string += trait["Description"]
                cost += trait["Point Cost"]

            traits_string += "\r\n"

        traits_string += "Total Race Cost: " + str(cost)
        race_string += attr_string
        race_string += traits_string
        return race_string

###
# Developed by MeridarchGekkota using
# Musicus Meter rules.
###
